# ci-deb-repo

How does it work?

1. create yml file as
  ```
  app_name:
    extends:
      - .build-package
    variables:
      PKG_NAME: app_name
      PKG_URL: https://url_url/url/app_name.git
      PKG_COMMIT: 9d7b29d01ebfb3e8cc75a847f004484d4a24970a  # hash you want to use
  ```

2. register the yml file inside the `.gitlab-ci.yml`
  ```
  include:
  ...
    - local: '/app_name.yml'
  ```
3. optionally add debian repository into `src/app_name/debian`
4. push your changes
5. CI will donwload source code, optionally combine with `src/app_name/debian` and build packages for defined architectures
6. after build it'll add these debian packages into the repository in specific branch
7. inside Mesa3D CI you can use commit at which are files uploaded into git, on your computer you can just follow latest files from the branch
