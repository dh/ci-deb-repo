#!/usr/bin/env bash
# © David Heidelberg, Collabora Ltd.
# SPDX-License-Identifier: BSD-3-Clause
set -ex
export DEBIAN_FRONTEND=noninteractive

echo "Debian: ${DEBIAN_DISTRO:?}"
echo "Arch: ${DEBIAN_ARCH:?}"

rm -f ./*.deb ./*.buildinfo ./*.changes

# shellcheck disable=SC2207
files=($(git diff-tree --no-commit-id --name-only -r "$CI_COMMIT_SHA"))

for file in "${files[@]}"; do
  [[ "$file" == "${PKG_NAME:?}.yml" ]] || [[ "$file" == "src/${PKG_NAME}" ]] && file_found=1
done

[[ -z "$file_found" ]] && {
  echo "Nothing to do."
  exit 0
}

# TODO: add i386
if [ "$DEBIAN_ARCH" != "amd64" ] && [ "$DEBIAN_ARCH" != "arm64" ] && [ "$DEBIAN_ARCH" != "armhf" ]; then  # add i386
  [[ -z "$PKG_CROSS" ]] && {
    echo "Skipping crosscompilation."
    exit 0
  }
fi

(
  echo "repositories:"
  echo "  ${PKG_NAME:?}:"
  echo "    type: ${PKG_VCS:-git}"
  echo "    url: ${PKG_URL:?}"
  echo "    version: ${PKG_COMMIT:?}"
) > sources.repos


vcs import --recursive --input "sources.repos" ./

[ -d "src/$PKG_NAME/debian" ] && cp -r "src/$PKG_NAME/debian" "$PKG_NAME/debian"

.gitlab-ci/build.sh
