#!/usr/bin/env bash
# © David Heidelberg, Collabora Ltd.
# SPDX-License-Identifier: BSD-3-Clause

export DEBIAN_FRONTEND=noninteractive

set -ex

REPOSITORY="$(printf "%s" "${CI_PROJECT_NAMESPACE}/" | tr / _)"
BRANCH="${DEBIAN_DISTRO:?}"

{
  git branch -D "${BRANCH}" || true  # GitLab does agressive caching
  git remote remove origin_gitlab || true
  git remote add origin_gitlab "https://${GITLAB_REPO_AUTH}@gitlab.freedesktop.org/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}.git"

  git fetch origin_gitlab "${BRANCH}" || {
    echo "Branch doesn't exist, create!"
    git switch --orphan "${BRANCH}"
  }

  git lfs fetch origin_gitlab "${BRANCH}" || {
    echo "See ^ or different issue with LFS"
  }

  [[ "$(git branch --show-current)" != "${BRANCH}" ]] && git checkout -b "${BRANCH}" origin_gitlab/"${BRANCH}"
}

# setup reprepro
{
  mkdir -p conf
  cat <<EOF > conf/distributions
Origin: gitlab.freedesktop.org/gfx-ci/ci-deb-repo/-/raw/${BRANCH}/
Label: gitlab.freedesktop.org/gfx-ci/ci-deb-repo/-/raw/${BRANCH}/
Codename: ${DEBIAN_DISTRO}
Architectures: amd64 arm64 i386 armhf s390x ppc64el
Components: main
Description: repository for Mesa3D CI
EOF
}

# add the debian files
{
  git lfs track "*.deb"

  for file in *.deb; do
    reprepro -b ./ includedeb "${DEBIAN_DISTRO}" "$file"
  done
  rm -f ./*.deb ./*.changes ./*.buildinfo sources.repo
}

# Generate README
# shellcheck disable=SC2006
# shellcheck disable=SC2086
{
  cat <<EOF > README.md
\`\`\`bash
echo "deb [trusted=yes] https://gitlab.freedesktop.org/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}/-/raw/${BRANCH}/ ${DEBIAN_DISTRO} main" | sudo tee /etc/apt/sources.list.d/${REPOSITORY}.list
\`\`\`
EOF
}

# commit and push into the repository
{
  git add README.md conf db dists pool
  git -c user.name="GitLab bot" -c user.email=none commit --message="Generated from $(git rev-parse --short origin/"$CI_COMMIT_BRANCH")"
  git push origin_gitlab "${BRANCH}"
}
